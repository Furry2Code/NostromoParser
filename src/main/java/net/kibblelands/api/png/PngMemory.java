package net.kibblelands.api.png;

import net.kibblelands.api.json.Transformer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PngMemory extends Transformer {
    private final byte[] png;

    public PngMemory( byte[] png) throws IOException {
        this.png = png;
    }

    public ByteArrayOutputStream write() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(this.png);
        return byteArrayOutputStream;
    }
}
