package net.kibblelands.api.json;

public class ModJsonTransformer extends Transformer {
    private Boolean status;
    private String[] mod_info;

    public Boolean getStatus() {
        return status;
    }

    public String[] getMod_info() {
        return mod_info;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setMod_info(String[] mod_info) {
        this.mod_info = mod_info;
    }
}
