package net.kibblelands.api.json;

public class ErrorJsonTransformer extends Transformer {
    private Boolean status;
    private String error;

    public String getError() {
        return error;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
