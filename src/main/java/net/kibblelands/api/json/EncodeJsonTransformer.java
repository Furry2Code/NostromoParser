package net.kibblelands.api.json;

public class EncodeJsonTransformer extends Transformer {
    private boolean status;
    private String encoded_password;

    public String getEncoded_password() {
        return encoded_password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setEncoded_password(String encoded_password) {
        this.encoded_password = encoded_password;
    }
}
