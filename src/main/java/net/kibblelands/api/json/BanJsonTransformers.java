package net.kibblelands.api.json;

public class BanJsonTransformers extends Transformer{
    private boolean status;
    private String[] data;
    private String bans;

    public String getBans() {
        return bans;
    }

    public String[] getData() {
        return data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setBans(String bans) {
        this.bans = bans;
    }

    public void setData(String[] data) {
        this.data = data;
    }
}
