package net.kibblelands.api.json;

public class MuteJsonTransformer extends Transformer {
    private boolean status;
    private String[] data;
    private String mutes;

    public String getMutes() {
        return mutes;
    }

    public String[] getData() {
        return data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setMutes(String mute) {
        this.mutes = mute;
    }

    public void setData(String[] data) {
        this.data = data;
    }
}
