package net.kibblelands.api.utils;

import net.kibblelands.api.json.*;
import net.kibblelands.api.png.PngMemory;

public enum EnumAction {
    AUTH("POST","/authenticate", null),
    MOD_CHECH("GET","/mod/%s/%s", ModJsonTransformer.class),
    ENCODE("GET","/encode/%s/%s", EncodeJsonTransformer.class),
    HEAD("GET","/head/%s/%s", PngMemory.class),

    SANCTION_BAN("GET","/sanction/bans", BanJsonTransformers.class),
    SANCTION_BAN_ID("GET","/sanction/bans/%s", BanJsonTransformers.class),
    SANCTION_BAN_POST("POST","/sanction/bans", BanJsonTransformers.class),
    SANCTION_BAN_PUT("PUT","/sanction/bans/", BanJsonTransformers.class),
    SANCTION_MUTE("GET","/sanction/mutes", MuteJsonTransformer.class),
    SANCTION_MUTE_ID("GET","/sanction/mutes/%s", MuteJsonTransformer.class),
    SANCTION_MUTE_POST("POST","/sanction/mutes", MuteJsonTransformer.class),
    SANCTION_MUTE_PUT("PUT","/sanction/mutes/", MuteJsonTransformer.class),

    SOCIALS_TWITTER_AUTHORIZATION_REQUEST("GET","/socials/twitter/authorization/request", null),
    SOCIALS_TWITTER_AUTHORIZATION_RESPONSE("GET", "/socials/twitter/authorization/response", null),
    SOCIALS_TWITTER_TWEETS_LIKED("GET", "/socials/twitter/tweets/liked", null);

    private final String type;
    private final String route;
    private final Class<? extends Transformer> transformer;

    EnumAction(String type, String route, Class<? extends Transformer> transformer) {
        this.type = type;
        this.route = route;
        this.transformer = transformer;
    }

    public String getType() {
        return type;
    }
    public String getRoute() {
        return route;
    }

    public Class<? extends Transformer> getTransformer() {
        return transformer;
    }
}
