package net.kibblelands.api.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.kibblelands.api.NostromoParser;
import net.kibblelands.api.json.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class URLUtils {
    private static ErrorJsonTransformer transformer = null;
    private static BanJsonTransformers banJsonTransformers = null;
    private static EncodeJsonTransformer encodeJsonTransformer = null;
    private static ModJsonTransformer modJsonTransformer = null;
    private static MuteJsonTransformer muteJsonTransformer = null;

    public static void parser(EnumAction action, String... input) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        HttpURLConnection stream = setupRequest(action, input);
        InputStreamReader reader = new InputStreamReader(stream.getInputStream());
        if (stream.getResponseCode() > 200 && stream.getResponseCode() < 200) {
            transformer = gson.fromJson(reader, ErrorJsonTransformer.class);
            stream.disconnect();
        } else {
           if (action.getTransformer() == BanJsonTransformers.class) {
               banJsonTransformers = gson.fromJson(reader, BanJsonTransformers.class);
               stream.disconnect();
           } else if (action.getTransformer() == EncodeJsonTransformer.class) {
               encodeJsonTransformer = gson.fromJson(reader, EncodeJsonTransformer.class);
               stream.disconnect();
           } else if (action.getTransformer() == ModJsonTransformer.class) {
               modJsonTransformer = gson.fromJson(reader,ModJsonTransformer.class);
               stream.disconnect();
           } else if (action.getTransformer() == MuteJsonTransformer.class) {
               muteJsonTransformer = gson.fromJson(reader,MuteJsonTransformer.class);
               stream.disconnect();
           }
        }
        transformer = gson.fromJson(reader, ErrorJsonTransformer.class);
        stream.disconnect();
    }

    private static HttpURLConnection setupRequest(EnumAction action, String... input) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(String.format(NostromoParser.API_URL + action.getRoute(), (Object) input)).openConnection();
        conn.setRequestMethod(action.getType().toUpperCase());
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0");
        return conn;
    }

    public static ErrorJsonTransformer getTransformer() {
        return transformer;
    }

    public static BanJsonTransformers getBanJsonTransformers() {
        return banJsonTransformers;
    }

    public static EncodeJsonTransformer getEncodeJsonTransformer() {
        return encodeJsonTransformer;
    }

    public static ModJsonTransformer getModJsonTransformer() {
        return modJsonTransformer;
    }

    public static MuteJsonTransformer getMuteJsonTransformer() {
        return muteJsonTransformer;
    }
}
